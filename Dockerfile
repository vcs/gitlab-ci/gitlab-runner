ARG RUNNER_VERSION
FROM gitlab/gitlab-runner:${RUNNER_VERSION}

# This will help with debugging docker-machine
RUN chmod g+w /home

# We need to be able to write in /etc/ssl/certs to inject CERN CA at runtime
RUN apt update && \
    chmod g+w /etc/ssl/certs

COPY dockermachine/bin/docker-machine /usr/bin/docker-machine