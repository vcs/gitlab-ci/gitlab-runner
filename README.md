# Gitlab-runner

This project keeps a gitlab-runner image with CERN requirements in order to work with out current infraestrusture.

As of July 2019, we have to build our own `docker-machine` binary to include latest changes that were not yet added to a release.

# Upgrading and versioning

Please refer to the [upgrade procedure](https://gitlabdocs.web.cern.ch/gitlabdocs/operations/upgraderunners/) for further details.